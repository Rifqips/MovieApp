package id.rifqipadisiliwangi.core.di

import com.chuckerteam.chucker.api.ChuckerInterceptor
import id.rifqipadisiliwangi.core.data.datasource.MovieDataSource
import id.rifqipadisiliwangi.core.data.datasource.MovieDataSourceImpl
import id.rifqipadisiliwangi.core.data.datasource.PagingDataSource
import id.rifqipadisiliwangi.core.data.local.database.ApplicationDatabase
import id.rifqipadisiliwangi.core.data.remote.client.NetworkClient
import id.rifqipadisiliwangi.core.data.remote.interceptor.ApplicationInterceptor
import id.rifqipadisiliwangi.core.data.remote.service.ApiEndpoint
import id.rifqipadisiliwangi.core.domain.repository.MovieRepository
import id.rifqipadisiliwangi.core.domain.repository.MovieRepositoryImpl
import id.rifqipadisiliwangi.core.domain.usecase.MovieInteractor
import id.rifqipadisiliwangi.core.domain.usecase.MovieUseCase
import org.koin.android.ext.koin.androidContext
import org.koin.core.module.Module
import org.koin.dsl.module

object CoreModule{
    val networkModules = module{
        single { ApplicationInterceptor() }
        single { ChuckerInterceptor.Builder(androidContext()).build() }
        single { NetworkClient(get(), get()) }
        single<ApiEndpoint>{get<NetworkClient>().create()}
    }

    val sourceModule = module{
        single{ MovieDataSourceImpl(get()) }
        single { PagingDataSource(get(), get()) }
    }
    val repositoryModule = module{
        single<MovieRepository>{ MovieRepositoryImpl(get(),get()) }
    }
    val interactorModule = module{
        single{ MovieInteractor(get())}
    }
    private val localModule = module {
        single { ApplicationDatabase.getInstance(get()) }
        single { get<ApplicationDatabase>().wishlistDao() }
        single { get<ApplicationDatabase>().notificationDao() }
        single { get<ApplicationDatabase>().cartDao() }
    }

    val coreModule: List<Module> = listOf(
        networkModules, sourceModule, repositoryModule, interactorModule, localModule,
    )
}