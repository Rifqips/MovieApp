package id.rifqipadisiliwangi.core.utils

import id.rifqipadisiliwangi.core.data.local.database.cart.CartKey
import id.rifqipadisiliwangi.core.data.local.database.movie.MovieKey
import id.rifqipadisiliwangi.core.data.remote.data.nowplaying.ResponseNowPlaying
import id.rifqipadisiliwangi.core.data.remote.data.popular.ResponsePopular
import id.rifqipadisiliwangi.core.data.remote.data.popular.ResultPopular
import id.rifqipadisiliwangi.core.domain.model.cart.KeyCart
import id.rifqipadisiliwangi.core.domain.model.nowplaying.NowPlayingResponse
import id.rifqipadisiliwangi.core.domain.model.popular.PopularResponse
import id.rifqipadisiliwangi.core.utils.DataMapper.toNowPlayingMapper
import id.rifqipadisiliwangi.core.utils.DataMapper.toPopularMapper
import id.rifqipadisiliwangi.core.utils.DataMapper.toPopulerRemote

object DataMapper {

    fun ResponseNowPlaying.toNowPlayingMapper() = NowPlayingResponse(
        dates = this.dates, page = this.page, results = this.results, totalPages = this.totalPages, totalResults = this.totalResults
    )


    fun ResponsePopular.toPopularMapper() = PopularResponse(
        page = this.page, results = this.results, totalPages = this.totalPages, totalResults = this.totalResults
    )

    fun ResultPopular.toPopulerRemote() = MovieKey(
        id = this.id, adult = this.adult, backdropPath = this.backdropPath, originalLanguage = this.originalLanguage, originalTitle = this.originalTitle, overview = this.overview, popularity = this.popularity, posterPath = this.posterPath, releaseDate = this.releaseDate, title = this.title, video = this.video, voteAverage = this.voteAverage, voteCount = this.voteCount
    )
    fun ResponsePopular.toLocalListData() = results.map { item -> item.toPopulerRemote()  }.toList()

    fun MovieKey.toLocaleUiData()= KeyCart(
        id = this.id, adult = this.adult, backdropPath = this.backdropPath, originalLanguage = this.originalLanguage, originalTitle = this.originalTitle, overview = this.overview, popularity = this.popularity, posterPath = this.posterPath, releaseDate = this.releaseDate, title = this.title, video = this.video, voteAverage = this.voteAverage, voteCount = this.voteCount
    )
}