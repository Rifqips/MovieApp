package id.rifqipadisiliwangi.core.domain.usecase

import androidx.paging.PagingData
import androidx.paging.map
import id.rifqipadisiliwangi.core.data.local.database.cart.CartKey
import id.rifqipadisiliwangi.core.data.local.database.movie.MovieKey
import id.rifqipadisiliwangi.core.domain.model.cart.KeyCart
import id.rifqipadisiliwangi.core.domain.model.nowplaying.NowPlayingResponse
import id.rifqipadisiliwangi.core.domain.model.popular.PopularResponse
import id.rifqipadisiliwangi.core.domain.repository.MovieRepository
import id.rifqipadisiliwangi.core.domain.state.UiState
import id.rifqipadisiliwangi.core.utils.DataMapper.toLocaleUiData
import id.rifqipadisiliwangi.core.utils.safeDataCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map

interface MovieUseCase {

    suspend fun getNowPlaying(
        apikey:String? = null,
        lang:String? = null,
        pageItem:Int? = null,
    ): NowPlayingResponse

    suspend fun getPopular(
        apikey:String? = null,
        lang:String? = null,
        pageItem:Int? = null
    ): PopularResponse

    suspend fun fetchLocalProducts(): Flow<UiState<PagingData<KeyCart>>>
}

class MovieInteractor(private val repository : MovieRepository) : MovieUseCase {


    override suspend fun getNowPlaying(
        apikey: String?,
        lang: String?,
        pageItem: Int?
    ): NowPlayingResponse {
        return  repository.getNowPlaying(apikey, lang, pageItem)
    }

    override suspend fun getPopular(
        apikey: String?,
        lang: String?,
        pageItem: Int?
    ): PopularResponse {
        return  repository.getPopular(apikey, lang, pageItem)
    }

    override suspend fun fetchLocalProducts(): Flow<UiState<PagingData<KeyCart>>> = safeDataCall{
        repository.fetchProductsLocal().map { data ->
            val mapped = data.map { productEntity: MovieKey -> productEntity.toLocaleUiData()  }
            UiState.Success(mapped)
        }.flowOn(Dispatchers.IO).catch { throwable -> UiState.Error(throwable) }
    }
}