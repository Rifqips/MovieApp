package id.rifqipadisiliwangi.core.domain.model.cart

import androidx.room.ColumnInfo
import androidx.room.PrimaryKey

data class KeyCart(
    val id: Int,
    val adult: Boolean,
    val backdropPath: String,
    val originalLanguage: String,
    val originalTitle: String,
    val overview: String,
    val popularity: Double,
    val posterPath: String,
    val releaseDate: String,
    val title: String,
    val video: Boolean,
    val voteAverage: Double,
    val voteCount: Int,
    var isChecked: Boolean = false,
)
