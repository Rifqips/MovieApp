package id.rifqipadisiliwangi.core.domain.repository

import androidx.paging.PagingData
import id.rifqipadisiliwangi.core.data.datasource.MovieDataSourceImpl
import id.rifqipadisiliwangi.core.data.datasource.PagingDataSource
import id.rifqipadisiliwangi.core.data.local.database.cart.CartKey
import id.rifqipadisiliwangi.core.data.local.database.movie.MovieKey
import id.rifqipadisiliwangi.core.domain.model.nowplaying.NowPlayingResponse
import id.rifqipadisiliwangi.core.domain.model.popular.PopularResponse
import id.rifqipadisiliwangi.core.utils.DataMapper.toNowPlayingMapper
import id.rifqipadisiliwangi.core.utils.DataMapper.toPopularMapper
import id.rifqipadisiliwangi.core.utils.safeApiCall
import id.rifqipadisiliwangi.core.utils.safeDataCall
import kotlinx.coroutines.flow.Flow

interface MovieRepository {


    suspend fun getNowPlaying(
        apikey:String? = null,
        lang:String? = null,
        pageItem:Int? = null,
    ): NowPlayingResponse

    suspend fun getPopular(
        apikey:String? = null,
        lang:String? = null,
        pageItem:Int? = null
     ): PopularResponse

    suspend fun fetchProductsLocal(): Flow<PagingData<MovieKey>>

}

class MovieRepositoryImpl(private val datasource : MovieDataSourceImpl, private val paging: PagingDataSource) : MovieRepository {

    override suspend fun getNowPlaying(apiKey : String?, lang: String?, pageItem: Int?): NowPlayingResponse = safeApiCall { datasource.getNowPlaying(apiKey = apiKey,lang = lang, pageItem = pageItem).toNowPlayingMapper() }

    override suspend fun getPopular(apiKey : String?, lang: String?, pageItem: Int?): PopularResponse = safeApiCall { datasource.getPopular(apiKey = apiKey, lang = lang, pageItem = pageItem).toPopularMapper() }

    override suspend fun fetchProductsLocal(): Flow<PagingData<MovieKey>> = safeDataCall {
        paging.fetchProducts()
    }
}