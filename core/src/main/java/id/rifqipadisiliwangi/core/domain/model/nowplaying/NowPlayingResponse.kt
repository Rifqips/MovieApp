package id.rifqipadisiliwangi.core.domain.model.nowplaying

import android.os.Parcelable
import androidx.annotation.Keep
import id.rifqipadisiliwangi.core.data.remote.data.nowplaying.Dates
import id.rifqipadisiliwangi.core.data.remote.data.nowplaying.ResultNowPlaying
import kotlinx.parcelize.Parcelize

@Keep
@Parcelize
data class NowPlayingResponse(
    val dates: Dates,
    val page: Int,
    val results: List<ResultNowPlaying>,
    val totalPages: Int,
    val totalResults: Int
) : Parcelable
