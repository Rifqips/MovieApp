package id.rifqipadisiliwangi.core.domain.model.popular

import android.os.Parcelable
import androidx.annotation.Keep
import id.rifqipadisiliwangi.core.data.remote.data.popular.ResultPopular
import kotlinx.parcelize.Parcelize

@Keep
@Parcelize
data class PopularResponse(
    val page: Int,
    val results: List<ResultPopular>,
    val totalPages: Int,
    val totalResults: Int
) : Parcelable
