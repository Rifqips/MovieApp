package id.rifqipadisiliwangi.core.data.remote.interceptor


import id.rifqipadisiliwangi.core.BuildConfig.API_KEY
import id.rifqipadisiliwangi.core.utils.Constant.NOW_PLAYING
import id.rifqipadisiliwangi.core.utils.Constant.POPULAR
import okhttp3.Interceptor
import okhttp3.Response

class ApplicationInterceptor: Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()

        val modifiedRequest = when (request.url.encodedPath){
            NOW_PLAYING, POPULAR -> {
                request
                    .newBuilder()
                    .addHeader("api_key", API_KEY)
                    .build()
            }

            else -> {
                request
                    .newBuilder()
                    .build()
            }
        }
        return chain.proceed(modifiedRequest)
    }
}
