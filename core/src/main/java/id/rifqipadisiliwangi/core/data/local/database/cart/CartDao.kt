package id.rifqipadisiliwangi.core.data.local.database.cart

import androidx.lifecycle.LiveData
import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import id.rifqipadisiliwangi.core.data.local.database.movie.MovieKey
import id.rifqipadisiliwangi.core.data.local.database.movie.RemoteKeys
import kotlinx.coroutines.flow.Flow


@Dao
interface CartDao {

    @Query("UPDATE cart_key SET isChecked = :isChecked WHERE id = :id ")
    fun isChecked(id: String, isChecked: Boolean)

    @Query("UPDATE cart_key SET isChecked = :newValue")
    suspend fun updateBooleanColumnAll(newValue: Boolean)

    @Query("UPDATE cart_key SET id = :quantity WHERE id = :id ")
    fun quantityCart(id: String, quantity: Int)

    @Query("UPDATE cart_key SET isChecked = :isChecked")
    fun checkAll(isChecked: Boolean)

//    @Query(
//        "INSERT OR REPLACE INTO cart_key (id, " +
//                "adult," +
//                "backdrop_path, " +
//                "original_language, " +
//                "original_title, " +
//                "overview, " +
//                "popularity, " +
//                "poster_path, " +
//                "release_date, " +
//                "title," +
//                "video," +
//                "vote_average," +
//                "vote_count," +
//                "isChecked," +
//                "poster_path) values (:id, :adult, :backdropPath,:originalLanguage, :originalTitle, :overview, :popularity, :posterPath, :releaseDate, :title, :video, :voteAverage, :voteCount, :isChecked)"
//    )
//    fun addCartList(
//         id: Int,
//         adult: Boolean,
//         backdropPath: String,
//         originalLanguage: String,
//         originalTitle: String,
//         overview: String,
//         popularity: Double,
//         posterPath: String,
//         releaseDate: String,
//         title: String,
//         video: Boolean,
//         voteAverage: Double,
//         voteCount: Int,
//         isChecked: Boolean,
//    )

    @Query("DELETE FROM cart_key WHERE id = :id")
    fun deleteProduct(id: String)

    @Query("DELETE FROM cart_key")
    fun deleteAllCart()

    @Delete
    suspend fun deleteAllCheckedProduct(cartEntity: List<CartKey>)

    @Query("SELECT * FROM cart_key")
    fun getCartList(): LiveData<List<CartKey>>

    @Query("SELECT * FROM cart_key WHERE id = :id")
    fun getCartById(id: String): Flow<List<CartKey>>



    /***
     * for paging data
     */

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertProducts(product: List<MovieKey>)

    @Query("SELECT * FROM cart_key")
    fun retrieveAllProducts():PagingSource<Int, MovieKey>

    @Query("DELETE FROM cart_key")
    suspend fun deleteAll()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(remoteKey: List<RemoteKeys>)

    @Query("SELECT * FROM remote_keys WHERE id = :id")
    suspend fun getRemoteKeysId(id:String): RemoteKeys?

    @Query("DELETE FROM remote_keys")
    suspend fun deleteAllKeys()
}