package id.rifqipadisiliwangi.core.data.remote.service

import id.rifqipadisiliwangi.core.data.remote.data.nowplaying.ResponseNowPlaying
import id.rifqipadisiliwangi.core.data.remote.data.popular.ResponsePopular
import id.rifqipadisiliwangi.core.utils.Constant.NOW_PLAYING
import id.rifqipadisiliwangi.core.utils.Constant.POPULAR
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface ApiEndpoint {

    @GET(NOW_PLAYING)
    suspend fun getNowPlaying(
        @Query("api_key") api_key:String? = null,
        @Query("language") lang:String? = null,
        @Query("page") page:Int? = null,
    ): ResponseNowPlaying


    @GET(POPULAR)
    suspend fun getPopular(
        @Query("language") lang:String? = null,
        @Query("page") page:Int? = null,
        @Query("api_key") apiKey:String? = null,
    ): ResponsePopular
}