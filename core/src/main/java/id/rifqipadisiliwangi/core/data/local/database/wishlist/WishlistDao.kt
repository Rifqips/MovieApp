package id.rifqipadisiliwangi.core.data.local.database.wishlist

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Query
import id.rifqipadisiliwangi.core.data.local.database.cart.CartKey
import kotlinx.coroutines.flow.Flow

@Dao
interface WishlistDao {


    @Query("UPDATE wishlist_key SET isChecked = :isChecked WHERE id = :id ")
    fun isChecked(id: String, isChecked: Boolean)

    @Query("UPDATE wishlist_key SET isChecked = :newValue")
    suspend fun updateBooleanColumnAll(newValue: Boolean)

    @Query("UPDATE wishlist_key SET id = :quantity WHERE id = :id ")
    fun quantityCart(id: String, quantity: Int)

    @Query("UPDATE wishlist_key SET isChecked = :isChecked")
    fun checkAll(isChecked: Boolean)

    @Query(
        "INSERT OR REPLACE INTO wishlist_key (id, " +
                "adult," +
                "backdrop_path, " +
                "original_language, " +
                "original_title, " +
                "overview, " +
                "popularity, " +
                "poster_path, " +
                "release_date, " +
                "title," +
                "video," +
                "vote_average," +
                "vote_count," +
                "isChecked) values (:id, :adult, :backdropPath,:originalLanguage, :originalTitle, :overview, :popularity, :posterPath, :releaseDate, :title, :video, :voteAverage, :voteCount, :isChecked)"
    )
    fun addCartList(
        id: Int,
        adult: Boolean,
        backdropPath: String,
        originalLanguage: String,
        originalTitle: String,
        overview: String,
        popularity: Double,
        posterPath: String,
        releaseDate: String,
        title: String,
        video: Boolean,
        voteAverage: Double,
        voteCount: Int,
        isChecked: Boolean,
    )

    @Query("DELETE FROM wishlist_key WHERE id = :id")
    fun deleteProduct(id: String)

    @Query("DELETE FROM wishlist_key")
    fun deleteAllCart()

    @Delete
    suspend fun deleteAllCheckedProduct(cartEntity: List<CartKey>)

    @Query("SELECT * FROM wishlist_key")
    fun getCartList(): LiveData<List<CartKey>>

    @Query("SELECT * FROM wishlist_key WHERE id = :id")
    fun getCartById(id: String): Flow<List<CartKey>>
}