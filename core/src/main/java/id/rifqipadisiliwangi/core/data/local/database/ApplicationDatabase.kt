package id.rifqipadisiliwangi.core.data.local.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import id.rifqipadisiliwangi.core.data.local.database.cart.CartDao
import id.rifqipadisiliwangi.core.data.local.database.cart.CartKey
import id.rifqipadisiliwangi.core.data.local.database.movie.MovieKey
import id.rifqipadisiliwangi.core.data.local.database.notification.NotificationDao
import id.rifqipadisiliwangi.core.data.local.database.notification.NotificationKey
import id.rifqipadisiliwangi.core.data.local.database.wishlist.WishlistDao
import id.rifqipadisiliwangi.core.data.local.database.wishlist.WishlistKey
import id.rifqipadisiliwangi.core.data.local.database.movie.RemoteKeys


@Database(
    entities = [CartKey::class, WishlistKey::class, NotificationKey::class, RemoteKeys::class, MovieKey::class],
    version = 2,
    exportSchema = false,
)
abstract class ApplicationDatabase : RoomDatabase() {
    abstract fun wishlistDao(): WishlistDao
    abstract fun cartDao(): CartDao
    abstract fun notificationDao(): NotificationDao
    companion object{
        private var INSTANCE : ApplicationDatabase? = null
        fun getInstance(context: Context):ApplicationDatabase? {
            if (INSTANCE == null){
                synchronized(ApplicationDatabase::class){
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                        ApplicationDatabase::class.java,"Application.db").build()
                }
            }
            return INSTANCE
        }
        fun destroyInstance(){
            INSTANCE = null
        }
    }
}