package id.rifqipadisiliwangi.core.data.datasource

import id.rifqipadisiliwangi.core.data.remote.data.nowplaying.ResponseNowPlaying
import id.rifqipadisiliwangi.core.data.remote.data.popular.ResponsePopular
import id.rifqipadisiliwangi.core.data.remote.service.ApiEndpoint
import id.rifqipadisiliwangi.core.utils.safeApiCall
import retrofit2.http.Query

interface MovieDataSource {
    suspend fun getNowPlaying(
        apikey:String? = null,
        lang:String? = null,
        pageItem:Int? = null,
    ): ResponseNowPlaying

    suspend fun getPopular(
        apikey:String? = null,
        lang:String? = null,
        pageItem:Int? = null,
    ): ResponsePopular

}
class MovieDataSourceImpl(private val api : ApiEndpoint) : MovieDataSource {

    override suspend fun getNowPlaying(apiKey : String?, lang: String?, pageItem: Int?): ResponseNowPlaying = safeApiCall { api.getNowPlaying(api_key = apiKey,lang = lang, page = pageItem) }

    override suspend fun getPopular(apiKey : String?, lang: String?, pageItem: Int?): ResponsePopular =  safeApiCall { api.getPopular(apiKey = apiKey,lang = lang, page = pageItem) }
}