package id.rifqipadisiliwangi.core.data.local.database.notification

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query


@Dao
interface NotificationDao {

    @Query("UPDATE notification_key SET isChecked = :isChecked WHERE notifId = :id ")
    fun notifIsChecked(id: Int, isChecked: Boolean)

    @Query("SELECT * FROM notification_key WHERE isChecked = :isChecked")
    fun getUnreadNotifications(isChecked: Boolean): LiveData<List<NotificationKey>?>

    @Query(
        "INSERT INTO notification_key (notifId," +
                "notifType," +
                "notifTitle, " +
                "notifBody, " +
                "notifDate, " +
                "notifTime, " +
                "notifImage, " +
                "isChecked) values (:notifId, :notifType, :notifTitle, :notifBody, :notifDate, :notifTime, :notifImage, :isChecked)"
    )
    suspend fun createNotification(
        notifId: Int,
        notifType: String,
        notifTitle: String,
        notifBody: String,
        notifDate: String,
        notifTime: String,
        notifImage: String,
        isChecked: Boolean
    )

    @Query("SELECT * FROM notification_key")
    fun getNotifications(): LiveData<List<NotificationKey>?>

    @Query("DELETE FROM notification_key")
    fun deleteAllNotif()
}