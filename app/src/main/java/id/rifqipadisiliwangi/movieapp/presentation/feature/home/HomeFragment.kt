package id.rifqipadisiliwangi.movieapp.presentation.feature.home

import android.util.Log
import android.view.LayoutInflater
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.os.bundleOf
import androidx.core.view.GravityCompat
import androidx.navigation.fragment.findNavController
import androidx.paging.PagingData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager2.widget.ViewPager2
import coil.load
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import id.rifqipadisiliwangi.core.base.BaseFragment
import id.rifqipadisiliwangi.core.domain.model.cart.KeyCart
import id.rifqipadisiliwangi.core.domain.model.corousel.CorouselDataItem
import id.rifqipadisiliwangi.core.domain.state.onError
import id.rifqipadisiliwangi.core.domain.state.onLoading
import id.rifqipadisiliwangi.core.domain.state.onSuccess
import id.rifqipadisiliwangi.core.utils.launchAndCollectIn
import id.rifqipadisiliwangi.movieapp.R
import id.rifqipadisiliwangi.movieapp.databinding.FragmentHomeBinding
import id.rifqipadisiliwangi.movieapp.databinding.HeaderNavigationDrawerBinding
import id.rifqipadisiliwangi.movieapp.presentation.common.adapter.corousel.CorouselAdapter
import id.rifqipadisiliwangi.movieapp.presentation.common.adapter.nowplaying.AdapterNowPlaying
import id.rifqipadisiliwangi.movieapp.presentation.common.adapter.popular.AdapterPagingItem
import id.rifqipadisiliwangi.movieapp.presentation.common.adapter.popular.AdapterPopularItem
import id.rifqipadisiliwangi.movieapp.presentation.common.viewmodel.home.HomeViewModel
import id.rifqipadisiliwangi.movieapp.utils.Constant.API_KEY_MOVIE_DB
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeFragment : BaseFragment<FragmentHomeBinding, HomeViewModel>(FragmentHomeBinding::inflate) {

    override val viewModel: HomeViewModel by viewModel()

    private lateinit var navHeaderBinding: HeaderNavigationDrawerBinding
    private lateinit var toggle: ActionBarDrawerToggle
    private lateinit var vpOnboarding: ViewPager2
    private lateinit var adapter: CorouselAdapter
    private lateinit var imageList: ArrayList<CorouselDataItem>
    private lateinit var tabs: TabLayout

    private var pagingData: PagingData<KeyCart>? = null

    private val adapterPagingPopular by lazy {
        AdapterPagingItem{}
    }

    private val adapterNowPlaying : AdapterNowPlaying by lazy {
        AdapterNowPlaying{}
    }
    private val adapterPopular : AdapterPopularItem by lazy {
        AdapterPopularItem{}
    }

    override fun initView() {
        setUpvp()
        navHeaderBinding = HeaderNavigationDrawerBinding.inflate(LayoutInflater.from(binding.navigationView.context))
        toggle = ActionBarDrawerToggle(requireActivity(), binding.homeDrawerLayout, R.string.open, R.string.close)
        binding.homeDrawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        setUpDrawer()
        with(binding){
            ivUser.load("https://getwallpapers.com/wallpaper/full/1/8/7/109525.jpg")
        }
    }

    private fun setUpDrawer(){
        with(binding){
            navigationView.addHeaderView(navHeaderBinding.root)
            navigationView.setNavigationItemSelectedListener { menuItem ->
                when (menuItem.itemId) {
                    R.id.wishlist_item -> activity?.supportFragmentManager?.findFragmentById(R.id.container_navigation)?.findNavController()?.navigate(R.id.action_homeFragment_to_wishlistFragment)
                    R.id.history_item -> activity?.supportFragmentManager?.findFragmentById(R.id.container_navigation)?.findNavController()?.navigate(R.id.action_homeFragment_to_historyFragment)
                    R.id.cart_item -> activity?.supportFragmentManager?.findFragmentById(R.id.container_navigation)?.findNavController()?.navigate(R.id.action_homeFragment_to_cartFragment)
                    else -> {}
                }
                homeDrawerLayout.closeDrawer(GravityCompat.START)
                true
            }
        }
    }

    override fun initListener() {
        with(binding){
            menuToolbar.setNavigationOnClickListener {
                homeDrawerLayout.open()
            }
            menuToolbar.setOnMenuItemClickListener{ menu ->
                when(menu.itemId){
                    R.id.search_item ->{}
                    R.id.iv_user -> {}
                }
                true

            }
        }
    }
    override fun observeData() {
        listObserveData()
        viewModel.fetchLocalMoviePopular()
//        viewModel.getListPopular(lang = "en-US", apikey = API_KEY_MOVIE_DB)
//        viewModel.listPopular.launchAndCollectIn(viewLifecycleOwner){ state ->
//            state.onLoading {  }
//                .onSuccess { data ->
//                    with(binding){
//                        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
//                        rvPopular.layoutManager = layoutManager
//                        rvPopular.adapter = adapterPopular
//                        adapterPopular.setData(data.results)
//                    }
//                }
//
//        }
    }
    private fun setUpvp() {
        vpOnboarding = binding.vpHome
        tabs = binding.tabs

        imageList = arrayListOf(
            CorouselDataItem("https://getwallpapers.com/wallpaper/full/a/b/e/75026.jpg","Your Gateway to Endless Entertainment!","Join the Vidio community and unlock a universe"),
            CorouselDataItem("https://image.tmdb.org/t/p/w500/1E5baAaEse26fej7uHcjOgEE2t2.jpg","Your Personalized Entertainment Destination Awaits!","Welcome to Vidio, where entertainment "),
            CorouselDataItem("https://getwallpapers.com/wallpaper/full/c/a/9/75047.jpg","Where Every Moment Sparks Joy!","Enter a realm of boundless entertainment with Vidio! Immerse !"),
        )

        adapter = CorouselAdapter(imageList)
        vpOnboarding.adapter = adapter

        TabLayoutMediator(
            tabs,
            vpOnboarding
        ) { tab, _ ->
            tab.customView = layoutInflater.inflate(R.layout.custom_tab_layout, null)
        }.attach()

        vpOnboarding.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
            }
        })
    }


    fun listObserveData(){
        with(viewModel){
            fetchLocalMoviePopular().launchAndCollectIn(viewLifecycleOwner){state->
                this.launch {
                    delay(1000)
                    state.onLoading {}
                        .onSuccess { data ->
                            pagingData = data
                            adapterPagingPopular.submitData(viewLifecycleOwner.lifecycle, data)
                        }.onError { error ->
                            Log.d("errorpaging", error.toString())
                        }
                }
            }
        }
    }


}