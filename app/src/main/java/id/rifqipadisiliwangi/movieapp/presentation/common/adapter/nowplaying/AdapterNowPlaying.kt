package id.rifqipadisiliwangi.movieapp.presentation.common.adapter.nowplaying

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import coil.load
import id.rifqipadisiliwangi.core.data.remote.data.nowplaying.ResultNowPlaying
import id.rifqipadisiliwangi.movieapp.databinding.ItemNowPlayingBinding
import id.rifqipadisiliwangi.movieapp.presentation.common.adapter.ViewHolderBinder
import id.rifqipadisiliwangi.movieapp.utils.Constant

class AdapterNowPlaying(private val onClickListenr : (ResultNowPlaying) -> Unit) : RecyclerView.Adapter<ViewHolder>() {

    private val dataDiffer = AsyncListDiffer(
        this,
        object : DiffUtil.ItemCallback<ResultNowPlaying>() {
            override fun areItemsTheSame(oldItem: ResultNowPlaying, newItem: ResultNowPlaying): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: ResultNowPlaying, newItem: ResultNowPlaying): Boolean {
                return oldItem.hashCode() == newItem.hashCode()
            }
        }
    )

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return when (viewType) {
            else -> {
                LinearMenuItemViewHolder(
                    binding = ItemNowPlayingBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    ),
                    onClickListenr
                )
            }
        }
    }

    override fun getItemCount(): Int = dataDiffer.currentList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        (holder as ViewHolderBinder<ResultNowPlaying>).bind(dataDiffer.currentList[position])
    }

    class LinearMenuItemViewHolder(
        private val binding: ItemNowPlayingBinding,
        private val onClickListener: (ResultNowPlaying) -> Unit
    ) : RecyclerView.ViewHolder(binding.root),
        ViewHolderBinder<ResultNowPlaying> {
        @SuppressLint("SetTextI18n")
        override fun bind(item: ResultNowPlaying) {
            with(binding){
                ivNowPlaying.load(Constant.TMDb_BACKDROP_PATH + item.backdropPath)
                tvTitle.text = item.originalTitle
            }

            binding.root.setOnClickListener {
                onClickListener(item)
            }
        }
    }
}