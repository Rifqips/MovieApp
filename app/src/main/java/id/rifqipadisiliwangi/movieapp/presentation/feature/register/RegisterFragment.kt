package id.rifqipadisiliwangi.movieapp.presentation.feature.register

import androidx.navigation.fragment.findNavController
import coil.load
import id.rifqipadisiliwangi.core.base.BaseFragment
import id.rifqipadisiliwangi.movieapp.R
import id.rifqipadisiliwangi.movieapp.databinding.FragmentRegisterBinding
import id.rifqipadisiliwangi.movieapp.presentation.common.viewmodel.prelogin.PreloginViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class RegisterFragment : BaseFragment<FragmentRegisterBinding, PreloginViewModel>(FragmentRegisterBinding::inflate) {

    override val viewModel: PreloginViewModel by viewModel()

    override fun initView() {
        with(binding){
            ivBackgroundRegister.load("https://wallpaperaccess.com/full/2052704.jpg")
        }
    }

    override fun initListener() {
        with(binding){
           btnSignUp.setOnClickListener {
               findNavController().navigate(R.id.action_registerFragment_to_profileFragment)
           }
            tvSignIn.setOnClickListener {
                findNavController().navigateUp()
            }
        }
    }

    override fun observeData() {}
}