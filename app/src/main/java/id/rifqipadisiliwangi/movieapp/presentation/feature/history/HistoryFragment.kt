package id.rifqipadisiliwangi.movieapp.presentation.feature.history

import id.rifqipadisiliwangi.core.base.BaseFragment
import id.rifqipadisiliwangi.movieapp.databinding.FragmentHistoryBinding
import id.rifqipadisiliwangi.movieapp.presentation.common.viewmodel.prelogin.PreloginViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class HistoryFragment : BaseFragment<FragmentHistoryBinding, PreloginViewModel>(FragmentHistoryBinding::inflate) {

    override val viewModel: PreloginViewModel by viewModel()

    override fun initView() {}

    override fun initListener() {}

    override fun observeData() {}

}