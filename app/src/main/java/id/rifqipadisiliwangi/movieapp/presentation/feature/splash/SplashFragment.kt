package id.rifqipadisiliwangi.movieapp.presentation.feature.splash

import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import id.rifqipadisiliwangi.core.base.BaseFragment
import id.rifqipadisiliwangi.movieapp.R
import id.rifqipadisiliwangi.movieapp.databinding.FragmentSplashBinding
import id.rifqipadisiliwangi.movieapp.presentation.common.viewmodel.prelogin.PreloginViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class SplashFragment :
    BaseFragment<FragmentSplashBinding, PreloginViewModel>(FragmentSplashBinding::inflate) {
    override val viewModel: PreloginViewModel by viewModel()

    override fun initView() {
        lifecycleScope.launch {
            delay(2000)
            findNavController().navigate(R.id.action_splashFragment_to_onboardingFragment)
        }
    }

    override fun initListener() {
        with(binding){
            tvTitleTheater.text = getString(R.string.string_title)
            tvSplash.text = getString(R.string.streing_streem_it)
        }
    }

    override fun observeData() {}

}