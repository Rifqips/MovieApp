package id.rifqipadisiliwangi.movieapp.presentation.feature.wishlist

import id.rifqipadisiliwangi.core.base.BaseFragment
import id.rifqipadisiliwangi.movieapp.databinding.FragmentWishlistBinding
import id.rifqipadisiliwangi.movieapp.presentation.common.viewmodel.prelogin.PreloginViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class WishlistFragment : BaseFragment<FragmentWishlistBinding, PreloginViewModel>(FragmentWishlistBinding::inflate) {

    override val viewModel: PreloginViewModel by viewModel()

    override fun initView() {}

    override fun initListener() {}

    override fun observeData() {}
}