package id.rifqipadisiliwangi.movieapp.presentation.feature.cart

import id.rifqipadisiliwangi.core.base.BaseFragment
import id.rifqipadisiliwangi.movieapp.databinding.FragmentCartBinding
import id.rifqipadisiliwangi.movieapp.presentation.common.viewmodel.prelogin.PreloginViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class CartFragment : BaseFragment<FragmentCartBinding, PreloginViewModel>(FragmentCartBinding::inflate) {

    override val viewModel: PreloginViewModel by viewModel()

    override fun initView() {}

    override fun initListener() {}

    override fun observeData() {}
}