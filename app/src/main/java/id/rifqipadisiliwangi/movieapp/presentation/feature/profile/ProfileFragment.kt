package id.rifqipadisiliwangi.movieapp.presentation.feature.profile

import coil.load
import id.rifqipadisiliwangi.core.base.BaseFragment
import id.rifqipadisiliwangi.movieapp.databinding.FragmentProfileBinding
import id.rifqipadisiliwangi.movieapp.presentation.common.viewmodel.prelogin.PreloginViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class ProfileFragment : BaseFragment<FragmentProfileBinding, PreloginViewModel>(FragmentProfileBinding::inflate) {

    override val viewModel: PreloginViewModel by viewModel()
    lateinit var sUsername : String
    lateinit var sPassword : String
    lateinit var sEmail : String
    lateinit var sUrl : String
    lateinit var sTelepon : String

    override fun initView() {
        with(binding){
            ivBackgroundProfile.load("https://wallpaperaccess.com/full/2052704.jpg")
        }
    }

    override fun initListener() {}

    override fun observeData() {}
}