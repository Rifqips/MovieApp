package id.rifqipadisiliwangi.movieapp.presentation.common.adapter.popular

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import coil.load
import id.rifqipadisiliwangi.core.data.remote.data.popular.ResultPopular
import id.rifqipadisiliwangi.movieapp.databinding.ItemPopularMoviesBinding
import id.rifqipadisiliwangi.movieapp.presentation.common.adapter.ViewHolderBinder
import id.rifqipadisiliwangi.movieapp.utils.Constant


class AdapterPopularItem(private val onClickListenr : (ResultPopular) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val dataDiffer = AsyncListDiffer(
        this,
        object : DiffUtil.ItemCallback<ResultPopular>() {
            override fun areItemsTheSame(oldItem: ResultPopular, newItem: ResultPopular): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: ResultPopular, newItem: ResultPopular): Boolean {
                return oldItem.hashCode() == newItem.hashCode()
            }
        }
    )

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            else -> {
                LinearMenuItemPopularViewHolder(
                    binding = ItemPopularMoviesBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    ),
                    onClickListenr
                )
            }
        }
    }

    override fun getItemCount(): Int = dataDiffer.currentList.size

    fun setData(data: List<ResultPopular>) {
        dataDiffer.submitList(data)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolderBinder<ResultPopular>).bind(dataDiffer.currentList[position])
    }

    class LinearMenuItemPopularViewHolder(
        private val binding: ItemPopularMoviesBinding,
        private val onClickListener: (ResultPopular) -> Unit
    ) : RecyclerView.ViewHolder(binding.root),
        ViewHolderBinder<ResultPopular> {
        @SuppressLint("SetTextI18n")
        override fun bind(item: ResultPopular) {
            with(binding){
                ivNowPlaying.load(Constant.TMDb_BACKDROP_PATH + item.backdropPath)
                tvTitle.text = item.originalTitle
            }

            binding.root.setOnClickListener {
                onClickListener(item)
            }
        }
    }
}