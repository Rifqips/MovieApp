package id.rifqipadisiliwangi.movieapp.presentation.feature.login

import androidx.navigation.fragment.findNavController
import coil.load
import id.rifqipadisiliwangi.core.base.BaseFragment
import id.rifqipadisiliwangi.movieapp.R
import id.rifqipadisiliwangi.movieapp.databinding.FragmentLoginBinding
import id.rifqipadisiliwangi.movieapp.presentation.common.viewmodel.prelogin.PreloginViewModel

class LoginFragment : BaseFragment<FragmentLoginBinding, PreloginViewModel>(FragmentLoginBinding::inflate) {

    override val viewModel: PreloginViewModel
        get() = TODO("Not yet implemented")

    override fun initView() {
        with(binding){
            ivBackgroundLogin.load("https://wallpaperaccess.com/full/2052704.jpg")
        }
    }

    override fun initListener() {
        with(binding){
            tvRegister.setOnClickListener {
                findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
            }
            btnLogin.setOnClickListener {
                findNavController().navigate(R.id.action_loginFragment_to_homeFragment)
            }
        }
    }

    override fun observeData() {}
}