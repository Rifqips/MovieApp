package id.rifqipadisiliwangi.movieapp.presentation.common.viewmodel.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.rifqipadisiliwangi.core.domain.model.nowplaying.NowPlayingResponse
import id.rifqipadisiliwangi.core.domain.model.popular.PopularResponse
import id.rifqipadisiliwangi.core.domain.state.UiState
import id.rifqipadisiliwangi.core.domain.usecase.MovieInteractor
import id.rifqipadisiliwangi.core.utils.asMutableStateFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class HomeViewModel(
    private val interactor: MovieInteractor
) : ViewModel() {

    private val _listNowPlaying : MutableStateFlow<UiState<NowPlayingResponse>> = MutableStateFlow(UiState.Empty)
    val listNowPlaying = _listNowPlaying.asStateFlow()

    private val _listPopular : MutableStateFlow<UiState<PopularResponse>> = MutableStateFlow(UiState.Empty)
    val listPopular = _listPopular.asStateFlow()

    fun fetchLocalMoviePopular() = runBlocking {
        interactor.fetchLocalProducts()
    }


    fun getListPopular(
        lang: String? = null,
        apikey: String? = null,
    ){
        viewModelScope.launch {
            _listPopular.asMutableStateFlow {
                interactor.getPopular(lang = lang, apikey = apikey)
            }
        }
    }

    fun getListNowPlaying(lang : String, pageItem : Int) {
        viewModelScope.launch {
            _listNowPlaying.asMutableStateFlow {
                interactor.getNowPlaying(pageItem = pageItem)
            }
        }
    }
}