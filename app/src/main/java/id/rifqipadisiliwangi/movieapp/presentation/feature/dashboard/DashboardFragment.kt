package id.rifqipadisiliwangi.movieapp.presentation.feature.dashboard

import id.rifqipadisiliwangi.core.base.BaseFragment
import id.rifqipadisiliwangi.movieapp.databinding.FragmentDashboardBinding
import id.rifqipadisiliwangi.movieapp.presentation.common.viewmodel.prelogin.PreloginViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class DashboardFragment : BaseFragment<FragmentDashboardBinding, PreloginViewModel>(FragmentDashboardBinding::inflate) {
    override val viewModel: PreloginViewModel by viewModel()

    override fun initView() {}

    override fun initListener() {}

    override fun observeData() {}

}