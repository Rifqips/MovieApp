package id.rifqipadisiliwangi.movieapp.di

import id.rifqipadisiliwangi.movieapp.presentation.common.viewmodel.home.HomeViewModel
import id.rifqipadisiliwangi.movieapp.presentation.common.viewmodel.prelogin.PreloginViewModel
import org.koin.androidx.viewmodel.dsl.viewModelOf
import org.koin.core.module.Module
import org.koin.dsl.module

object AppModules {

    private val viewModelModule = module {
        viewModelOf(::HomeViewModel)
        viewModelOf(::PreloginViewModel)
    }

    private val utilsModule = module {}

    val modules: List<Module> = listOf(
        viewModelModule,
        utilsModule,
    )
}
